package main

import (
	"aoc2022"
	"aoc2023"
	"aocgeneric"
	"fmt"
)

var year int = 2023
var day int = 4
var part int = 2
var test bool = false

// The most important part of any solution, the holiday spirit
func festive_greeter() {
	fmt.Println("Merry Christmas")
}

type library struct {
	year   int
	day    int
	solver func(string, int) int
}

func main() {
	festive_greeter()

	library := []library{
		{2022, 1, aoc2022.Day_1_solver},
		{2022, 2, aoc2022.Day_2_solver},
		{2022, 3, aoc2022.Day_3_solver},
		{2022, 4, aoc2022.Day_4_solver},
		{2022, 6, aoc2022.Day_6_solver},
		{2022, 7, aoc2022.Day_7_solver},
		{2022, 8, aoc2022.Day_8_solver},
		{2022, 9, aoc2022.Day_9_solver},
		{2022, 10, aoc2022.Day_10_solver},
		{2022, 11, aoc2022.Day_11_solver},
		{2022, 12, aoc2022.Day_12_solver},
		{2022, 13, aoc2022.Day_13_solver},
		{2023, 1, aoc2023.Day_1_solver},
		{2023, 2, aoc2023.Day_2_solver},
		{2023, 3, aoc2023.Day_3_solver},
		{2023, 4, aoc2023.Day_4_solver},
	}

	day_path := aocgeneric.Generate_day_path(year, day, test)

	var solver func(string, int) int
	for _, solver_set := range library {
		if year == 2022 && day == 5 {
			fmt.Println(aoc2022.Day_5_solver(day_path, part))
		}

		if solver_set.year == year && solver_set.day == day {
			solver = solver_set.solver
			fmt.Println(solver(day_path, part))
		}
	}

}
