package binary

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func Binary_to_decimal(_input []int) int {
	string_input := []string{}

	for _, val := range _input {
		string_input = append(string_input, fmt.Sprint(val))
	}

	single_string := strings.Join(string_input, "")

	decimal_out, err := strconv.ParseInt(single_string, 2, 64)
	if err != nil {
		return 0
	}

	return int(decimal_out)
}

func Invert(_input []int) []int {
	result := []int{}
	for _, value := range _input {
		result = append(result, Flip(value))
	}

	return result
}

func Flip(_input int) int {
	return int(math.Abs(float64(_input) - 1))
}
