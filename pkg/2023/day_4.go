package aoc2023

import (
	"functional"
	"maths"
	"parse/text"
	"set"
	"strconv"
	"strings"

	"golang.org/x/exp/maps"
)

// to be reused in day 2
func process_id_string(_input string) int {
	line_label := strings.Split(_input, ":")[0]
	id_str := last_number_multi_character(line_label)
	id, _ := strconv.Atoi(id_str)
	return id
}

func last_number_multi_character(_input string) string {
	split_input := strings.Split(_input, "")

	leftmost_index := find_number_furthest_index(split_input, len(split_input)-1, -1)

	used_string := strings.Join(split_input[leftmost_index:], "")
	return used_string
}

func space_delimited_list(_input string) []string {

	clean_input := strings.TrimSpace(_input)
	split_list := strings.Split(clean_input, " ")
	reduced_list, _ := functional.Filter_slice(split_list, is_not_empty_string)
	return reduced_list
}

func is_not_empty_string(_input string) (bool, error) {
	return _input != "", nil
}

func to_scratchcard(_input string) scratchcard {
	split := strings.Split(_input, "|")
	winning := space_delimited_list(split[0])
	played := space_delimited_list(split[1])

	return scratchcard{
		winning,
		played,
	}
}

func lottery_score(_input int) int {
	if _input == 0 {
		return 0
	}

	if _input == 1 {
		return 1
	}

	extra := _input - 1

	output := 1
	for i := 0; i < extra; i++ {
		output *= 2
	}

	return output

}

type scratchcard struct {
	winning []string
	played  []string
}

func sequence(_start int, _to int) []int {
	if _start > _to {
		return sequence(_to, _start)
	}

	output := []int{}
	for i := _start; i < _to; i++ {
		output = append(output, i)
	}
	return output
}

// maps are always pointers, so no need for return
func increment_games(_game_set map[int]int, _indexes []int, _by int) {
	for _, index := range _indexes {
		_game_set[index] += _by
	}
}

func Day_4_solver(_input string, _part int) int {
	lines, _ := text.Row_to_string_file_parser(_input)

	games := make(map[int]scratchcard)

	for _, line := range lines {
		split := strings.Split(line, ":")
		id := process_id_string(split[0])
		game := to_scratchcard(split[1])
		games[id] = game
	}

	if _part == 1 {
		scores := []int{}
		for _, game := range games {
			winners := len(set.Intersect(game.winning, game.played))
			scores = append(scores, lottery_score(winners))
		}
		return maths.Sum(scores)
	}

	if _part == 2 {
		scores := make(map[int]int) // share an integer index with games (defined at top)

		for id := range games {
			scores[id] = 1 // pre-allocate with 1 copy of each
		}

		for id := range lines { // ordering by the lines, as maps are not ordered

			game := games[id]

			wins := len(set.Intersect(game.winning, game.played))
			games_to_increment := sequence(id+1, id+wins+1) // you get copies of the *next* n games, where n is the number of wins
			increment_games(scores, games_to_increment, scores[id])
		}

		all_scores := maps.Values(scores)
		return maths.Sum(all_scores)
	}

	return 0
}
