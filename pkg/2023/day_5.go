package aoc2023

import (
	"fmt"
	"functional"
	"maths"
	"parse/text"
	"set"
	"strconv"
	"strings"
)

func set_map(_input map[int]int, _source_start int, _destination_start int, _range int) {
	for i := 0; i < _range; i++ {
		_input[_source_start+i] = _destination_start + i
	}
}

func get_value(_input map[int]int, _key int) int {
	res, ok := _input[_key]

	if !ok {
		return _key
	}

	return res
}

func get_chain(_map_of_maps map[string]map[int]int, _start int) int {
	var input int = _start
	var output int

	for _, key := range keys {
		current_map := _map_of_maps[key]
		output = get_value(current_map, input)
		input = output
	}

	return output
}

var keys []string = []string{
	"seed-to-soil",
	"soil-to-fertilizer",
	"fertilizer-to-water",
	"water-to-light",
	"light-to-temperature",
	"temperature-to-humidity",
	"humidity-to-location",
}

func Day_5_solver(_input string, _part int) int {
	lines, err := text.Row_to_string_file_parser(_input)
	if err != nil {
		fmt.Println(err)
		return 0
	}

	seeds_raw := lines[0]

	fmt.Println(seeds_raw)
	instructions_raw := lines[1:]

	map_of_maps := make(map[string]map[int]int)

	var current_map map[int]int
	for _, line := range instructions_raw {
		if line == "" {
			continue
		}

		reduced_line := strings.ReplaceAll(line, " map:", "")

		if set.Member(keys, reduced_line) {
			fmt.Println("new key:", reduced_line)
			new_map := make(map[int]int)
			current_map = new_map
			map_of_maps[reduced_line] = current_map
			continue
		}

		inputs := space_delimited_list(reduced_line)
		res, err := functional.Map_int(inputs, strconv.Atoi)
		fmt.Println("New match rule:", res, err)

		set_map(current_map, res[1], res[0], res[2])

	}

	inputs := []int{79, 14, 55, 13}
	outputs, _ := functional.Map_int(inputs, func(_in int) (int, error) {
		return get_chain(map_of_maps, _in), nil
	})

	return maths.Min(outputs)
}
