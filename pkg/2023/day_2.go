package aoc2023

import (
	"maths"
	"parse/text"
	"strconv"
	"strings"
)

type round struct {
	red   int
	green int
	blue  int
}

type game struct {
	id           int
	rounds       []round
	needed_red   int
	needed_green int
	needed_blue  int
}

func make_round(_input string) (round, error) {
	var output round
	marble_sets := strings.Split(_input, ",")

	for _, set := range marble_sets {
		clean_set := strings.TrimLeft(set, " ")
		set_pair := strings.Split(clean_set, " ")
		if len(set_pair) < 2 {
			continue
		}

		colour := set_pair[1]

		value, err := strconv.Atoi(set_pair[0])
		if err != nil {
			return round{}, err
		}

		if colour == "red" {
			output.red = value
		}

		if colour == "green" {
			output.green = value
		}

		if colour == "blue" {
			output.blue = value
		}

	}
	return output, nil
}

func make_rounds(_input string) []round {

	round_strings := strings.Split(_input, ";")

	rounds := []round{}
	for _, str := range round_strings {
		round, _ := make_round(str)
		rounds = append(rounds, round)
	}

	return rounds
}

func process_line(_input string) game {
	pair := strings.Split(_input, ":")
	if len(pair) < 2 {
		return game{}
	}
	id_string := pair[0]
	game_string := pair[1]

	id_set := strings.Split(id_string, " ")
	if len(id_set) == 0 {
		return game{}
	}

	if len(id_set) == 1 {
		return game{}
	}

	id, _ := strconv.Atoi(id_set[1])

	rounds := make_rounds(game_string)

	max_red := 0
	max_green := 0
	max_blue := 0

	for _, round := range rounds {
		if round.red > max_red {
			max_red = round.red
		}
		if round.green > max_green {
			max_green = round.green
		}
		if round.blue > max_blue {
			max_blue = round.blue
		}
	}

	return game{id, rounds, max_red, max_green, max_blue}

}

func possible(_input game) bool {
	var max_red int = 12
	var max_green int = 13
	var max_blue int = 14

	if _input.needed_red > max_red {
		return false
	}

	if _input.needed_green > max_green {
		return false
	}

	if _input.needed_blue > max_blue {
		return false
	}

	return true
}

func power(_input game) int {
	return _input.needed_red * _input.needed_green * _input.needed_blue
}

func Day_2_solver(_input string, _part int) int {
	lines, _ := text.Row_to_string_file_parser(_input)

	games := []game{}

	for _, line := range lines {
		game := process_line(line)
		games = append(games, game)
	}

	if _part == 1 {
		possible_ids := []int{}
		for _, game := range games {
			if possible(game) {
				possible_ids = append(possible_ids, game.id)
			}
		}

		return maths.Sum(possible_ids)

	}

	powers := []int{}
	for _, game := range games {
		powers = append(powers, power(game))
	}

	return maths.Sum(powers)
}
