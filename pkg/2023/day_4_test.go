package aoc2023

import (
	"parse/text"
	"set"
	"strings"
	"testing"
)

func Test_lottery_score(t *testing.T) {
	exp := [][]int{
		{1, 1},
		{2, 2},
		{3, 4},
		{4, 8},
		{5, 16},
		{6, 32},
		{7, 64},
		{8, 128},
		{9, 256},
		{10, 512},
	}

	for _, expect := range exp {
		if score := lottery_score(expect[0]); expect[1] != score {
			t.Log("Expected score failure, for: ", expect[0], "Wins, we expect a score of: ", expect[1], "But got a score of: ", score)
			t.Fail()
		}
	}

}

func Test_winning_numbers(t *testing.T) {
	lines, _ := text.Row_to_string_file_parser("../../input_files/2023/day_4_test_b.txt")
	split := strings.Split(lines[0], ":")
	game := to_scratchcard(split[1])
	winners := set.Intersect(game.winning, game.played)
	if len(winners) > len(game.winning) {
		t.Log("The winners group is larger than the full set of winning numbers")
		t.Fail()
	}

	if len(winners) != 10 {
		t.Log("Too many winning pairs, with", len(winners))
		t.Log("Winning numbers", winners)
		for key := range winners {
			t.Log("Winner ", key, ":", winners[key])
		}
		t.Fail()
	}

}

type id_expectation struct {
	file        string
	ids_present []int
}

func Test_id_parser(t *testing.T) {
	id_expectations := []id_expectation{
		{"../../input_files/2023/day_4_test_b.txt", []int{1, 2}},
		{"../../input_files/2023/day_4_test_c.txt", []int{194, 195}},
	}
	for _, exp := range id_expectations {
		id_parse_expectation(t, exp.file, exp.ids_present)
	}
}

func id_parse_expectation(t *testing.T, _input string, _expectation []int) {

	lines, _ := text.Row_to_string_file_parser(_input)

	ids := []int{}
	for _, line := range lines {
		id := process_id_string(line)
		ids = append(ids, id)
	}

	expected := _expectation
	if !set.Contains(ids, expected) {
		t.Log("Processed IDs missing members, needing: ", expected, " but got : ", ids)
		t.Fail()
	}

}

type sequence_expectation struct {
	start    int
	end      int
	expected []int
}

func Test_sequences(t *testing.T) {
	expectations := []sequence_expectation{
		{1, 2, []int{1, 2}},
		{1, 5, []int{1, 2, 3, 4, 5}},
		{4, 6, []int{4, 5, 6}},
		{-1, 2, []int{-1, 0, 1, 2}},
		{5, -1, []int{-1, 0, 1, 2, 3, 4, 5}},
		{0, 0, []int{0}},
	}

	for _, expectation := range expectations {
		if seq := sequence(expectation.start, expectation.end); set.Contains(seq, expectation.expected) {
			t.Logf("Incorrect sequence generation, expected %v, got %v", expectation.expected, seq)
			t.Fail()
		}
	}
}

func Test_incrementing(t *testing.T) {
	testing_map := make(map[int]int)

	indexes := []int{2, 5, 12, 1}
	initial_map := make(map[int]int)
	increment := 2
	increment_games(testing_map, indexes, increment)
	for key, val := range testing_map {
		diff := val - initial_map[key]
		if diff != increment {
			t.Logf("Post Increment difference is not as expected, difference is %v instead of %v", diff, increment)
			t.Fail()
		}
	}

}
