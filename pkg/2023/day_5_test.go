package aoc2023

import (
	"testing"
)

func Test_map_set(t *testing.T) {
	base_map := make(map[int]int)

	set_map(base_map, 98, 50, 2)
	set_map(base_map, 50, 52, 48)

	expects := map[int]int{
		0:   0,
		1:   1,
		48:  48,
		49:  49,
		50:  52,
		51:  53,
		96:  98,
		97:  99,
		98:  50,
		99:  51,
	}

	for input, output := range expects {
		if resp := get_value(base_map, input); resp != output {
			t.Logf("Expected %v and got %v", output, resp)
			t.Fail()
		}
	}
}
