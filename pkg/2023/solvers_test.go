package aoc2023

import (
	"aocgeneric"
	"fmt"
	"testing"
)

type expectation struct {
	year          int
	day           int
	part          int
	solver        func(string, int) int
	result        int
	override_path string
}

func Test_Solvers(t *testing.T) {

	var expectations []expectation = []expectation{
		{2023, 1, 1, Day_1_solver, 142, ""},
		{2023, 1, 2, Day_1_solver, 281, "../../input_files/2023/day_1_test_b.txt"},
		// {2023, 1, 1, Day_1_solver, 54450, "../../input_files/2023/day_1.txt"}, // Ignored because I changed the code rather than switch it
		{2023, 1, 2, Day_1_solver, 54265, "../../input_files/2023/day_1.txt"},

		{2023, 2, 1, Day_2_solver, 8, ""},
		{2023, 2, 2, Day_2_solver, 2286, ""},
		{2023, 2, 1, Day_2_solver, 2913, "../../input_files/2023/day_2.txt"},
		{2023, 2, 2, Day_2_solver, 55593, "../../input_files/2023/day_2.txt"},

		{2023, 3, 1, Day_3_solver, 4361, ""},
		{2023, 3, 1, Day_3_solver, 4361, "../../input_files/2023/day_3_test_b.txt"},
		{2023, 3, 1, Day_3_solver, 1627, "../../input_files/2023/day_3_test_c.txt"},
		{2023, 3, 2, Day_3_solver, 467835, ""},
		{2023, 3, 1, Day_3_solver, 512794, "../../input_files/2023/day_3.txt"},
		{2023, 3, 2, Day_3_solver, 67779080, "../../input_files/2023/day_3.txt"},

		{2023, 4, 1, Day_4_solver, 13, ""},
		{2023, 4, 1, Day_4_solver, 1024, "../../input_files/2023/day_4_test_b.txt"}, // Row1: 10 matches,  Row2: 10 matches
		{2023, 4, 1, Day_4_solver, 22193, "../../input_files/2023/day_4.txt"},
		{2023, 4, 2, Day_4_solver, 30, ""},
		{2023, 4, 2, Day_4_solver, 5625994, "../../input_files/2023/day_4.txt"},

		{2023, 5, 1, Day_5_solver, 35, ""},
	}

	for _, exp := range expectations {
		test_expectation(t, exp)
	}
}

func test_expectation(t *testing.T, e expectation) {
	var path string
	path = aocgeneric.Generate_day_path(e.year, e.day, true)
	if e.override_path != "" {
		path = e.override_path
	}

	result := e.solver(path, e.part)
	if result != e.result {
		t.Log("Issues with solver for day: " + fmt.Sprint(e.day) + " part: " + fmt.Sprint(e.part))
		t.Log("The expected value was: " + fmt.Sprint(e.result) + " but the solver returned: " + fmt.Sprint(result))
		t.Fail()
	}
}
