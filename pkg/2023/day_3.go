package aoc2023

import (
	"errors"
	"maths"
	"matrix"
	"parse/text"
	"set"
	"strconv"
	"strings"
)

/*

Solution plan:

Load all the input data into a matrix, logging all the symbol locations
using those locations search all around the symbol to find a number
If you find a number, recursively search to find its other elements
maintain a record of the relative position of each part of the number
stick them back together
Return all numbers


If implemented in some good helpers it would be a good candidate to retry day12 from 2022
*/

// _step: -1 for left search, +1 for right search
func find_number_furthest_index(_input []string, _start int, _step int) int {
	if _start == 0 && _step < 0 {
		return _start
	}
	if _start == len(_input)-1 && _step > 0 {
		return _start
	}

	index := _start + _step
	if len(_input) < index+1 {
		return _start
	}
	left_is_digit := set.Member(text.Digits, _input[index])
	if left_is_digit {
		return find_number_furthest_index(_input, index, _step)
	}
	return _start
}

func find_number(_input [][]string, _coord []int) int {
	var used_row []string
	var start_point int

	if len(_coord) != 0 && len(_input) != 0 {
		used_row = _input[_coord[0]]
	}

	if len(used_row) != 0 {
		start_point = _coord[1]
	}

	leftmost_index := find_number_furthest_index(used_row, start_point, -1)
	rightmost_index := find_number_furthest_index(used_row, start_point, 1)

	if len(used_row) == 0 {
		return 0
	}
	used_string := strings.Join(used_row[leftmost_index:rightmost_index+1], "")

	as_int, _ := strconv.Atoi(used_string)

	return as_int

}

func get_numbers(_input [][]string, _coord []int) []int {
	rows := []int{_coord[0] - 1, _coord[0], _coord[0] + 1}
	cols := []int{_coord[1] - 1, _coord[1], _coord[1] + 1}

	nums := []int{}

	for _, row := range rows {
		for _, col := range cols {
			val, err := matrix.Coord_get(_input, []int{row, col})
			if errors.Is(err, matrix.ErrOutOfBounds) {
				continue
			}
			if set.Member(text.Digits, val) {
				found := find_number(_input, []int{row, col})
				nums = append(nums, found)
			}
		}
	}

	return set.Unique(nums)

}

func to_matrix(_lines []string) [][]string {

	source_matrix := [][]string{}
	for _, line := range _lines {
		split_line := strings.Split(line, "")
		source_matrix = append(source_matrix, split_line)

	}
	return source_matrix
}

type selector func(string) bool

func any_non_digit(_input string) bool {
	if _input == "." {
		return false
	}
	if set.Member(text.Digits, _input) {
		return false
	}
	return true
}

func just_gears(_input string) bool {
	return _input == "*"
}

func get_symbol_coords(_matrix [][]string, _selector selector) [][]int {

	var symbol_coords [][]int
	for row, line := range _matrix {
		for col, val := range line {
			if _selector(val) {
				symbol_coords = append(symbol_coords, []int{row, col})
			}
		}
	}
	return symbol_coords
}

// Split out a 'to_matrix' function
// Split out a function which gets symbol coords from the matrix
func Day_3_solver(_input string, _part int) int {
	lines, _ := text.Row_to_string_file_parser(_input)
	source_matrix := to_matrix(lines)

	var symbol_coords [][]int

	if _part == 1 {
		symbol_coords = get_symbol_coords(source_matrix, any_non_digit)
	}

	if _part == 2 {
		symbol_coords = get_symbol_coords(source_matrix, just_gears)
	}

	all_nums := []int{}
	for _, val := range symbol_coords {
		found_nums := get_numbers(source_matrix, val)
		if _part == 1 { // add all numbers
			all_nums = append(all_nums, found_nums...)
		}

		if _part == 2 && len(found_nums) == 2 { // Only add valid gear ratios
			all_nums = append(all_nums, maths.Prod(found_nums))
		}
	}

	return maths.Sum(all_nums)
}
