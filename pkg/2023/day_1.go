package aoc2023

import (
	"maths"
	"parse/text"
	"strconv"
	"strings"
)

type text_span struct {
	start int
	len   int
}

func first_value(_input string, _value string) text_span {
	start := strings.Index(_input, _value)
	if start < 0 {
		return text_span{}
	}
	return text_span{start, len(_value)}
}

func last_value(_input string, _value string) text_span {
	start := strings.LastIndex(_input, _value)
	if start < 0 {
		return text_span{}
	}
	return text_span{start, len(_value)}
}

func get_substr(_input string, _span text_span) string {
	output := _input[_span.start : _span.start+_span.len]

	return output
}

func first_span(_input []text_span) text_span {
	earliest := 0 // defaults to choosing the first entry in an even list
	for key, val := range _input {
		if val.start < _input[earliest].start {
			earliest = key
		}
	}

	return _input[earliest]
}

// In this case last span is not the span which finishes last, but the one which starts last
func last_span(_input []text_span) text_span {
	earliest := 0 // defaults to choosing the first entry in an even list
	for key, val := range _input {
		if val.start > _input[earliest].start {
			earliest = key
		}
	}

	return _input[earliest]
}

var valid_nums []string = []string{
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"one",
	"two",
	"three",
	"four",
	"five",
	"six",
	"seven",
	"eight",
	"nine",
}

func first_number(_input string) string {

	first_matches := []text_span{}
	for _, val := range valid_nums {
		first := first_value(_input, val)
		if first.len == 0 {
			continue
		}
		first_matches = append(first_matches, first)
	}

	earliest_match := first_span(first_matches)

	return get_substr(_input, earliest_match)
}

func last_number(_input string) string {

	last_matches := []text_span{}
	for _, val := range valid_nums {
		last := last_value(_input, val)
		if last.len == 0 {
			continue
		}
		last_matches = append(last_matches, last)
	}

	latest_match := last_span(last_matches)

	return get_substr(_input, latest_match)
}

// func end_number_matcher(_input string) []string {
// 	var first string
// 	var last string

// 	return []string{first, last}
// }

func Day_1_solver(_path string, _part int) int {
	lines, _ := text.Row_to_string_file_parser(_path)

	nums := []int{}
	for _, val := range lines {
		num_string := first_number(val) + last_number(val)
		num_string = text.Convert_text_numbers_to_numerals(num_string)
		int, _ := strconv.Atoi(num_string)
		nums = append(nums, int)
	}

	return maths.Sum(nums)

}
