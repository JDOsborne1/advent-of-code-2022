package aoc2023

import "testing"

type small_expect struct {
	input    string
	expected string
}

func Test_first_instance(t *testing.T) {
	var expectations []small_expect = []small_expect{
		{
			"testwo3fiveighthrees",
			"two",
		},
		{
			"2twothreeights",
			"2",
		},
	}
	var output string

	for _, exp := range expectations {

		output = first_number(exp.input)
		if output != exp.expected {
			t.Log("Failed to get expected results, expecting", exp.expected, "recieved", output)
			t.Fail()
		}

	}

}

func Test_last_instance(t *testing.T) {
	var expectations []small_expect = []small_expect{
		{
			"testwo3fiveighthrees",
			"three",
		},
		{
			"2twothreeights",
			"eight",
		},
	}
	var output string

	for _, exp := range expectations {

		output = last_number(exp.input)
		if output != exp.expected {
			t.Log("Failed to get expected results, expecting", exp.expected, "recieved", output)
			t.Fail()
		}

	}

}
