package aoc2022

import (
	"maths"
	"parse/text"
	"regexp"
	"set"
	"strconv"
)

type coord struct {
	x int
	y int
}

func Day_9_solver(_source_path string, _part int) int {
	parsed_data, _ := text.Row_to_string_file_parser(_source_path)
	actions := []coord{}
	for _, val := range parsed_data {
		action := coord{0, 0}
		split_pattern := regexp.MustCompile(`([RULD])\s(\d+)`)
		matches := split_pattern.FindStringSubmatch(val)
		direction := matches[1]
		if direction == "R" {
			action = coord{1, 0}
		}
		if direction == "U" {
			action = coord{0, 1}
		}
		if direction == "L" {
			action = coord{-1, 0}
		}
		if direction == "D" {
			action = coord{0, -1}
		}

		amount, _ := strconv.Atoi(matches[2])

		for key := 0; key < amount; key++ {
			actions = append(actions, action)
		}
	}
	tail_positions := []coord{}
	head_pos := coord{0, 0}
	tail_pos := coord{0, 0}
	tail_vel := coord{0, 0}
	for _, act := range actions {
		head_pos = add_pos(head_pos, act)
		tail_pos = add_pos(tail_pos, tail_vel)
		tail_vel = trailing_tail_vector(head_pos, tail_pos) // could output this as the 'tail instructions' for later nesting

		tail_positions = append(tail_positions, tail_pos)
	}

	return len(set.Unique(tail_positions))
}

func add_pos(_start coord, _disp coord) coord {
	output := coord{}
	output.x = _start.x + _disp.x
	output.y = _start.y + _disp.y
	return output
}

func subt_pos(_start coord, _disp coord) coord {
	output := coord{}
	output.x = _start.x - _disp.x
	output.y = _start.y - _disp.y
	return output
}

func trailing_tail_vector(_head_pos coord, _tail_pos coord) coord {
	diff := subt_pos(_head_pos, _tail_pos)
	abs_diff := coord{
		maths.Abs(diff.x),
		maths.Abs(diff.y),
	}
	if maths.Max([]int{abs_diff.x, abs_diff.y}) <= 1 {
		return coord{0, 0}
	}
	if diff.x > 1 {
		diff.x = 1
	}
	if diff.y > 1 {
		diff.y = 1
	}
	if diff.x < -1 {
		diff.x = -1
	}
	if diff.y < -1 {
		diff.x = -1
	}

	return diff
}
