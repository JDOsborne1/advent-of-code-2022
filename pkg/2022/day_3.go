package aoc2022

import (
	"functional"
	"maths"
	"parse/text"
	"set"
	"strings"
)

func Day_3_solver(_source_path string, _part int) int {
	input_lines, err := text.Row_to_string_file_parser(_source_path)
	if err != nil {
		return 0
	}
	if _part == 1 {
		var first_compartment []string
		var second_compartment []string

		var common_items []string

		for _, val := range input_lines {
			split_string := break_string_in_half(val)
			first_compartment = strings.Split(split_string[0], "")
			second_compartment = strings.Split(split_string[1], "")
			common_item := set.Intersect(first_compartment, second_compartment)
			if len(common_item) > 1 {
				// fmt.Println("intersection more than 1 value", common_item)
				return 0
			}
			common_items = append(common_items, common_item[0])

		}

		priorities, _ := functional.Map_int(common_items, text.Letters_to_int)

		return maths.Sum(priorities)
	}

	if _part == 2 {
		elf_groups := make(map[int][]string)
		group := 1

		for _, val := range input_lines {
			if len(elf_groups[group]) == 3 {
				group++
			}
			elf_groups[group] = append(elf_groups[group], val)
		}

		group_badges := []string{}

		for _, elfs := range elf_groups {
			var backpacks [][]string
			for _, pack := range elfs {
				split_pack := strings.Split(pack, "")
				backpacks = append(backpacks, split_pack)
			}
			badge_letter := functional.Reduce(backpacks, set.Intersect[string])
			group_badges = append(group_badges, badge_letter[0])

		}

		group_badge_values, _ := functional.Map_int(group_badges, text.Letters_to_int)

		return maths.Sum(group_badge_values)
	}
	return 0

}

func break_string_in_half(_input string) []string {
	var first_half []byte
	var second_half []byte

	for key, val := range _input {
		if key < len(_input)/2 {
			first_half = append(first_half, byte(val))
		} else {
			second_half = append(second_half, byte(val))
		}

	}

	return []string{string(first_half), string(second_half)}
}
