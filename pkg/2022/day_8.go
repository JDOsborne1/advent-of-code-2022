package aoc2022

import (
	// "fmt"
	"functional"
	"maths"
	"matrix"
	"parse/text"
	"strconv"
	"strings"
	"topography"
)

func Day_8_solver(_source_path string, _part int) int {
	parsed_input, _ := text.Row_to_string_file_parser(_source_path)
	broken_input := [][]int{}
	for _, line := range parsed_input {
		split_line := strings.Split(line, "")
		int_line, _ := functional.Map_int(split_line, strconv.Atoi)
		broken_input = append(broken_input, int_line)
	}
	transposed_input := matrix.Transpose(broken_input)

	if _part == 1 {

		visibles := 0
		for x := 0; x < len(broken_input); x++ {
			for y := 0; y < len(broken_input[0]); y++ {
				x_cut := broken_input[y]
				y_cut := transposed_input[x]

				visible := topography.Left_visible(x_cut, x) ||
					topography.Left_visible(y_cut, y) ||
					topography.Right_visible(x_cut, x) ||
					topography.Right_visible(y_cut, y)

				if visible {
					visibles++
				}
			}
		}

		return visibles
	}

	if _part == 2 {
		scores := []int{}
		for x := 0; x < len(broken_input); x++ {
			for y := 0; y < len(broken_input[0]); y++ {
				x_cut := broken_input[y]
				y_cut := transposed_input[x]

				score := topography.Left_scenic(x_cut, x) *
					topography.Left_scenic(y_cut, y) *
					topography.Right_scenic(x_cut, x) *
					topography.Right_scenic(y_cut, y)

				scores = append(scores, score)

			}
		}
		return maths.Max(scores)

	}

	return 0

}
