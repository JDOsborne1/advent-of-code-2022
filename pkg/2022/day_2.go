package aoc2022

import (
	"maths"
	"parse/text"
	"strings"
)

func Day_2_solver(_source_path string, _part int) int {
	parsed_file, err := text.Row_to_string_file_parser(_source_path)
	if err != nil {
		return 0
	}
	scores := []int{}

	if _part == 1 {
		for _, val := range parsed_file {
			current_round_score := 0
			strategy := strings.Split(val, " ")

			switch strategy[1] {
			case "X":
				current_round_score += 1
			case "Y":
				current_round_score += 2
			case "Z":
				current_round_score += 3
			}

			switch strategy[0] {
			case "A":
				switch strategy[1] {
				case "X":
					current_round_score += 3
				case "Y":
					current_round_score += 6
				case "Z":
					current_round_score += 0
				}
			case "B":
				switch strategy[1] {
				case "X":
					current_round_score += 0
				case "Y":
					current_round_score += 3
				case "Z":
					current_round_score += 6
				}
			case "C":
				switch strategy[1] {
				case "X":
					current_round_score += 6
				case "Y":
					current_round_score += 0
				case "Z":
					current_round_score += 3
				}
			}

			scores = append(scores, current_round_score)

		}
		return maths.Sum(scores)
	}

	if _part == 2 {
		for _, val := range parsed_file {
			current_round_score := 0
			strategy := strings.Split(val, " ")
			switch strategy[1] {
			case "X":
				current_round_score += 0
			case "Y":
				current_round_score += 3
			case "Z":
				current_round_score += 6
			}

			switch strategy[0] {
			case "A":
				switch strategy[1] {
				case "X":
					current_round_score += 3
				case "Y":
					current_round_score += 1
				case "Z":
					current_round_score += 2
				}
			case "B":
				switch strategy[1] {
				case "X":
					current_round_score += 1
				case "Y":
					current_round_score += 2
				case "Z":
					current_round_score += 3
				}
			case "C":
				switch strategy[1] {
				case "X":
					current_round_score += 2
				case "Y":
					current_round_score += 3
				case "Z":
					current_round_score += 1
				}
			}

			scores = append(scores, current_round_score)

		}
	}
	return maths.Sum(scores)
}
