package aoc2022

import (
	"fmt"
	"parse/text"
	"strconv"
	"strings"
)

type operation struct {
	class     string
	intensity int
}

func Day_10_solver(_source_path string, _part int) int {
	parsed_input, _ := text.Row_to_string_file_parser(_source_path)

	// 	parsed_input := []string{
	// 		"noop",
	// "addx 3",
	// "addx -5",
	// 	}

	x := 1
	cycles := 0

	operations := make(map[int]operation)
	for _, val := range parsed_input {
		if val == "noop" {
			operations[cycles] = operation{"noop", 0}
			cycles++
			continue
		}
		spl_val := strings.Split(val, " ")
		op_name := spl_val[0]
		intensity, _ := strconv.Atoi(spl_val[1])
		if op_name == "addx" {
			operations[cycles] = operation{"addx", intensity}
			cycles += 2
		}
	}

	sample_points := []int{20, 60, 100, 140, 180, 220}

	x_register_history := make(map[int]int)
	skips := 0
	pending_value := 0
	for cycle := 0; cycle < cycles; cycle++ {
		x_register_history[cycle] = x
		op := operations[cycle]
		if skips != 0 {
			skips--
			if skips == 0 {
				x = pending_value
				pending_value = 0
			}
			continue
		}
		if op.class == "noop" {
			continue
		}
		if op.class == "addx" {
			skips++
			if pending_value != 0 {
				fmt.Println("WARNING: overrwriting the pending value")
			}
			pending_value = x + op.intensity
		}
	}
	x_register_history[cycles] = x
	// sig_strength := 1
	for _, sample_point := range sample_points {
		fmt.Println("Register x:", x_register_history[sample_point])
		fmt.Println(x_register_history[sample_point] * sample_point)
	}

	return skips
}
