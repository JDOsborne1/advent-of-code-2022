package aoc2022

import (
	"matrix"
	"parse/text"
	"regexp"
	"set"
	"strconv"
	"strings"
)

type move_command struct {
	from   int
	to     int
	amount int
}

func parse_move_command(_input string) move_command {
	pattern := regexp.MustCompile(`move (\d+) from (\d+) to (\d+)`)

	matches := pattern.FindStringSubmatch(_input)
	amount, _ := strconv.Atoi(matches[1])
	origin, _ := strconv.Atoi(matches[2])
	destination, _ := strconv.Atoi(matches[3])

	return move_command{
		from:   origin,
		to:     destination,
		amount: amount,
	}
}

func Day_5_solver(_source_path string, _part int) string {
	parsed_input, _ := text.Row_to_string_file_parser(_source_path)

	opening_space := regexp.MustCompile(`^\s\s\s\s`)
	crates := [][]string{}

	var split_point int

	// get all the crates data and pivot into shape
	for key, val := range parsed_input {
		clean_val := opening_space.ReplaceAllString(val, "[-] ")
		clean_val = strings.ReplaceAll(clean_val, "    ", " [-]")
		clean_val = strings.ReplaceAll(clean_val, "[", "")
		clean_val = strings.ReplaceAll(clean_val, "]", "")

		crates = append(crates, strings.Split(clean_val, " "))

		if val == "" {
			split_point = key
			break
		}
	}

	crates = crates[:len(crates)-2]
	t_crates := matrix.Transpose(crates)

	vertical_crates := [][]string{}
	for _, val := range t_crates {
		var reduced_outcome []string
		for _, val := range val {
			if val != "-" {
				reduced_outcome = append(reduced_outcome, val)
			}
		}
		vertical_crates = append(vertical_crates, reduced_outcome)
	}

	// Get all the move instructions and parse them
	move_commands := []move_command{}
	for key, val := range parsed_input {
		if key < split_point+1 {
			continue
		}
		move_commands = append(move_commands, parse_move_command(val))
	}
	var output string

	if _part == 1 {

		for _, cmd := range move_commands {
			source := vertical_crates[cmd.from-1]
			vertical_crates[cmd.from-1] = source[cmd.amount:]
			vertical_crates[cmd.to-1] = append(set.Reverse(source[:cmd.amount]), vertical_crates[cmd.to-1]...)

		}

	}

	if _part == 2 {
		for _, cmd := range move_commands {
			source := vertical_crates[cmd.from-1]
			vertical_crates[cmd.from-1] = source[cmd.amount:]
			vertical_crates[cmd.to-1] = append(set.Identity(source[:cmd.amount]), vertical_crates[cmd.to-1]...)
		}
	}

	for _, stack := range vertical_crates {
		output += stack[0]
	}

	return output
}
