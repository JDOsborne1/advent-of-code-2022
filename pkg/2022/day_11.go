package aoc2022

import (
	"maths"
	"parse/text"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"golang.org/x/exp/maps"
)

/*
These monkeys are data structures
They posses a reservoir of objects which have values
They can make decisions based on internal configuration
Using these decisions they transfer items from their reservoir to another monkeys

Their internal operation logic can be represented as a 1st class function
Their test logic can be represented as a function also

There is an 'observation effect' which changes the values on the objects held by the monkeys

To fully derive the monkeys from the input we need the following mappings:


Monkey n: -:- Create a new monkey, and set that as the actively parsed monkey

Starting items: x, y -:- For the actively parsed monkey, set their initial reservoir to have items with the following worry values [x, y]

Operation: new = [old|int] [+*-/] [old|int] -:- this needs to be parsed into a function, using some kind of 'factory' function

Test: divisible by [int] -:- This needs to be parsed into a function, again using a factory, though much simpler this time

If true: throw to monkey [int] -:- This needs to be parsed to set up a transfer action between the currently parsed monkey and another monkey (which may not currently exist) This transfer action is triggered when the above test function returns TRUE
If false: ...... -:- same as above but in the case where the test function is FALSE

The flow is:
Inspect
- Observation change
Test
Act

The observation change is fixed function (for now) of f(x) = floor(x/3)

The flow is repeated once per object in the reservoir, once every turn for each monkey
Each monkey gets 1 turn per round

Transfer rules: when an item is transferred between reservoirs, it goes from the front of the source, to the tail of the destination

*/

type monkey struct {
	reservoir []uint64
	action    func(uint64) uint64 // takes the old value and produces the new one
	condition func(uint64) bool   // takes the current value and tests it
	branch    map[bool]int
	divisor   int
}

func observation_effect(_input uint64) uint64 {
	return _input / 3
}

// func throw_item(_from monkey, _to monkey) (monkey, monkey) {
// 	_to.reservoir = append(_to.reservoir, _from.reservoir[0])
// 	_from.reservoir = _from.reservoir[1:]
// 	return _from, _to
// }

func Day_11_solver(_source_path string, _part int) int {
	parsed_input, _ := text.Row_to_string_file_parser(_source_path)

	monkeys := make(map[int]monkey)

	monkey_match := regexp.MustCompile(`Monkey (\d+)`)
	items_match := regexp.MustCompile(`Starting items:((\s\d+,?)*)`)
	throw_match := regexp.MustCompile(`If (\S+): throw to monkey (\d+)`)
	test_match := regexp.MustCompile(`Test: divisible by (\d+)`)
	action_match := regexp.MustCompile(`Operation: new = (\S+) ([+*]) (\S+)`)

	current_monkey := 0
	for _, val := range parsed_input {

		if monkey_match.MatchString(val) {
			monkey_name := monkey_match.FindStringSubmatch(val)[1]
			monkey_num, _ := strconv.Atoi(monkey_name)
			current_monkey = monkey_num
		}

		if items_match.MatchString(val) {
			items := items_match.FindStringSubmatch(val)
			reduced_items := strings.ReplaceAll(items[1], " ", "")
			individual_items := strings.Split(reduced_items, ",")
			int_items := []uint64{}
			for _, item := range individual_items {
				int_item, _ := strconv.Atoi(item)
				int_items = append(int_items, uint64(int_item))
			}
			// fmt.Println("Found some items", int_items)
			current_monkey_state := monkeys[current_monkey]
			current_monkey_state.reservoir = int_items
			monkeys[current_monkey] = current_monkey_state
		}

		if throw_match.MatchString(val) {
			matches := throw_match.FindStringSubmatch(val)
			destination_monkey, _ := strconv.Atoi(matches[2])
			var pass bool
			if matches[1] == "true" {
				pass = true
			}
			if matches[1] == "false" {
				pass = false
			}
			current_monkey_state := monkeys[current_monkey]
			if len(current_monkey_state.branch) == 0 {
				current_monkey_state.branch = make(map[bool]int)
			}

			current_monkey_state.branch[pass] = destination_monkey
			monkeys[current_monkey] = current_monkey_state
			// fmt.Println("found throw instruction", matches)
		}

		if test_match.MatchString(val) {
			test_val_str := test_match.FindStringSubmatch(val)[1]
			test_val, _ := strconv.Atoi(test_val_str)
			// fmt.Println("found a test, checking for divisibility by ", test_val)
			new_func := func(_input uint64) bool { return (_input % uint64(test_val)) == 0 }
			current_monkey_state := monkeys[current_monkey]
			current_monkey_state.condition = new_func
			current_monkey_state.divisor = test_val
			monkeys[current_monkey] = current_monkey_state
		}

		if action_match.MatchString(val) {
			match_list := action_match.FindStringSubmatch(val)

			var operator string
			var modifier string
			if len(match_list) >= 4 {
				operator = match_list[2]
				modifier = match_list[3]
			}

			old := modifier == "old"

			// fmt.Println("Found action", match_list)
			var used_func func(uint64) uint64
			if operator == "+" && old {
				used_func = func(_input uint64) uint64 { return _input + _input }
			}
			if operator == "*" && old {
				used_func = func(_input uint64) uint64 { return _input * _input }
			}
			if operator == "+" && !old {
				int_val, _ := strconv.Atoi(modifier)
				used_func = func(_input uint64) uint64 { return _input + uint64(int_val) }
			}
			if operator == "*" && !old {
				int_val, _ := strconv.Atoi(modifier)
				used_func = func(_input uint64) uint64 { return _input * uint64(int_val) }
			}
			current_monkey_state := monkeys[current_monkey]
			current_monkey_state.action = used_func
			monkeys[current_monkey] = current_monkey_state

		}

	}

	// fmt.Println(monkeys)

	rounds_lim := 20
	if _part == 2 {
		rounds_lim = 10000
	}

	muffle_value := 1
	for _, monkey := range monkeys {
		muffle_value *= int(monkey.divisor)
	}

	monkey_business := make(map[int]int)
	max_worry_level := uint64(0)
	// rounds loop
	for round := 0; round < rounds_lim; round++ {
		// monkey turn
		for name := 0; name < len(monkeys); name++ {
			// fmt.Println("currently monkey", name)
			// inspection
			for _, item := range monkeys[name].reservoir {
				monkey_business[name]++
				// fmt.Println("opening worry level", item)
				worry_level := monkeys[name].action(item)
				// fmt.Println("after inspection worry level", worry_level)
				if _part == 1 {
					worry_level = observation_effect(worry_level)
				}
				if _part == 2 {
					worry_level %= uint64(muffle_value)
				}

				if worry_level > max_worry_level {
					max_worry_level = worry_level
				}
				// fmt.Println("after bored worry level", worry_level)
				pass := monkeys[name].condition(worry_level)
				// fmt.Println("Does current item pass test:", pass)
				to_monkey := monkeys[name].branch[pass]

				monkey_state := monkeys[to_monkey]
				monkey_state.reservoir = append(monkey_state.reservoir, worry_level)
				monkeys[to_monkey] = monkey_state

			}
			monkey_state := monkeys[name]
			monkey_state.reservoir = []uint64{}
			monkeys[name] = monkey_state
		}
		// fmt.Println("round", round, "concludes")
		// if round == 999 {
		// 	// fmt.Println(monkey_business)
		// }
	}

	counts := maps.Values(monkey_business)
	sort.Ints(counts)

	return maths.Prod(counts[len(counts)-2:])
}
