package aoc2022

import (
	"functional"
	"maths"
	"parse/text"
	"strings"
)

/*

Parsing will be:
Input -> Matrix of letters
Matrix (letters) -> Matrix (numbers)

Numbers used to calculate links, based on relative coords

Instead of using an outside graph package (as one is not readily usable)
We can perform a very inefficient sink first detection.

Will need to loop in a while loop, and incrementally add a distance to the sink based on the smallest accessible distance to the source for each cell.

for each item in the matrix, check it's neigbours. If they have a distance to the sink already recorded, then the distance value for this cell is the smallest value of it's neigbours incremented by the cost (+1)

Repeat this until there are no unchecked cells, or until you have reached the source

*/

func Day_12_solver(_source_path string, _part int) int {

	parsed_input, _ := text.Row_to_string_file_parser(_source_path)

	source_matrix := [][]string{}
	for _, line := range parsed_input {
		split_line := strings.Split(line, "")
		source_matrix = append(source_matrix, split_line)
	}

	height_matrix := make([][]int, len(source_matrix)) // make a function to zero allocate an integer matrix of the same dimensions of an input matrix of another type

	source_coords := coord{}
	sink_coords := coord{}

	for y, vec := range source_matrix {
		height_matrix[y] = make([]int, len(vec))
		for x, item := range vec {
			if item == "S" {
				source_coords.x = x
				source_coords.y = y
				// fmt.Println("Found start at", source_coords)
				height_matrix[y][x] = 1
				continue
			}

			if item == "E" {
				sink_coords.x = x
				sink_coords.y = y
				// fmt.Println("Found end at", sink_coords)
				height_matrix[y][x] = 26
				continue
			}

			height_matrix[y][x], _ = text.Letters_to_int(item)
		}
	}

	candidate_sources := []coord{}

	if _part == 2 {
		for y, row := range height_matrix {
			for x, element := range row {
				if element == 1 {
					candidate_sources = append(candidate_sources, coord{x: x, y: y})
				}
			}
		}
	}

	if _part == 1 {
		candidate_sources = append(candidate_sources, source_coords)
	}
	// fmt.Println(candidate_sources)

	distances_matrix := make([][]int, len(source_matrix))
	for y := range distances_matrix {
		distances_matrix[y] = make([]int, len(source_matrix[y]))
		// for x := range distances_matrix[y] {
		// 	distances_matrix[x][x] = 1
		// }
	}
	distances_matrix[sink_coords.y][sink_coords.x] = 1

	const increment_value = 1
	solution_buffer := 3

	for {
		unvisited := false

		for y, row := range distances_matrix {
			for x, element := range row {
				if element == 0 {
					unvisited = true
				}
				options := []int{}
				current_height := height_matrix[y][x]

				// Search up
				if y+1 < len(distances_matrix) {
					up_valid := height_matrix[y+1][x] <= current_height+1
					if up_valid {
						up_option := distances_matrix[y+1][x]
						options = append(options, up_option)
					}
				}

				// search down
				if y-1 >= 0 {
					down_valid := height_matrix[y-1][x] <= current_height+1
					if down_valid {
						down_option := distances_matrix[y-1][x]
						options = append(options, down_option)
					}
				}

				// search left
				if x-1 >= 0 {
					left_valid := height_matrix[y][x-1] <= current_height+1
					if left_valid {
						left_option := distances_matrix[y][x-1]
						options = append(options, left_option)
					}
				}

				// search right
				if x+1 < len(distances_matrix[y]) {
					right_valid := height_matrix[y][x+1] <= current_height+1
					if right_valid {
						right_option := distances_matrix[y][x+1]
						options = append(options, right_option)
					}
				}

				reduced_options, _ := functional.Filter_slice(
					options,
					func(_input int) (bool, error) {
						return _input != 0, nil
					},
				)

				best_option := maths.Min(reduced_options)

				if distances_matrix[y][x] != 0 {
					distances_matrix[y][x] = maths.Min([]int{best_option + increment_value, distances_matrix[y][x]})
				} else if best_option != 0 {
					distances_matrix[y][x] = best_option + increment_value
				}

			}
		}
		// breaking out of loop once all areas have been visited
		if !unvisited {
			// fmt.Println("final loop")
			break
		}

		// breaking out of loop once the source coords are populated
		found := false
		for _, source := range candidate_sources {
			if distances_matrix[source.y][source.x] != 0 {
				found = true
			}
		}
		if found {
			solution_buffer--
		}

		if found && solution_buffer == 0 {
			// fmt.Println("final loop")
			break
		}
	}

	candidate_values := []int{}
	for _, source := range candidate_sources {
		candidate_values = append(candidate_values, distances_matrix[source.y][source.x])
	}
	non_zero_values, _ := functional.Filter_slice(
		candidate_values,
		func(_input int) (bool, error) {
			return _input != 0, nil
		},
	)

	return maths.Min(non_zero_values) - 1
}
