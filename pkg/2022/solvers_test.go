package aoc2022

import (
	"aocgeneric"
	"fmt"
	"testing"
)

type expectation struct {
	year          int
	day           int
	part          int
	solver        func(string, int) int
	result        int
	override_path string
}

func Test_Solvers(t *testing.T) {

	var expectations []expectation = []expectation{
		{2022, 1, 1, Day_1_solver, 24000, ""},
		{2022, 1, 2, Day_1_solver, 45000, ""},
		{2022, 1, 1, Day_1_solver, 72478, "../../input_files/2022/day_1.txt"},
		{2022, 1, 2, Day_1_solver, 210367, "../../input_files/2022/day_1.txt"},

		{2022, 2, 1, Day_2_solver, 15, ""},
		{2022, 2, 2, Day_2_solver, 12, ""},
		{2022, 2, 1, Day_2_solver, 9177, "../../input_files/2022/day_2.txt"},
		{2022, 2, 2, Day_2_solver, 12111, "../../input_files/2022/day_2.txt"},

		{2022, 3, 1, Day_3_solver, 157, ""},
		{2022, 3, 2, Day_3_solver, 70, ""},
		{2022, 3, 1, Day_3_solver, 7716, "../../input_files/2022/day_3.txt"},
		{2022, 3, 2, Day_3_solver, 2973, "../../input_files/2022/day_3.txt"},

		{2022, 6, 1, Day_6_solver, 7, ""},
		{2022, 6, 1, Day_6_solver, 5, "../../input_files/2022/day_6_test_2.txt"},
		{2022, 6, 1, Day_6_solver, 6, "../../input_files/2022/day_6_test_3.txt"},
		{2022, 6, 1, Day_6_solver, 10, "../../input_files/2022/day_6_test_4.txt"},
		{2022, 6, 1, Day_6_solver, 11, "../../input_files/2022/day_6_test_5.txt"},
		{2022, 6, 1, Day_6_solver, 1965, "../../input_files/2022/day_6.txt"},
		{2022, 6, 2, Day_6_solver, 19, ""},
		{2022, 6, 2, Day_6_solver, 23, "../../input_files/2022/day_6_test_2.txt"},
		{2022, 6, 2, Day_6_solver, 23, "../../input_files/2022/day_6_test_3.txt"},
		{2022, 6, 2, Day_6_solver, 29, "../../input_files/2022/day_6_test_4.txt"},
		{2022, 6, 2, Day_6_solver, 26, "../../input_files/2022/day_6_test_5.txt"},
		{2022, 6, 2, Day_6_solver, 2773, "../../input_files/2022/day_6.txt"},

		{2022, 7, 1, Day_7_solver, 95437, ""},
		{2022, 7, 1, Day_7_solver, 288384, "../../input_files/2022/day_7_test_2.txt"},
		{2022, 7, 2, Day_7_solver, 24933642, ""},
		{2022, 7, 1, Day_7_solver, 1667443, "../../input_files/2022/day_7.txt"},
		{2022, 7, 2, Day_7_solver, 8998590, "../../input_files/2022/day_7.txt"},

		{2022, 8, 1, Day_8_solver, 21, ""},
		{2022, 8, 2, Day_8_solver, 8, ""},
		{2022, 8, 1, Day_8_solver, 1538, "../../input_files/2022/day_8.txt"},
		{2022, 8, 2, Day_8_solver, 496125, "../../input_files/2022/day_8.txt"},

		{2022, 9, 1, Day_9_solver, 13, ""},

		{2022, 11, 1, Day_11_solver, 10605, ""},
		{2022, 11, 2, Day_11_solver, 2713310158, ""},
		{2022, 11, 1, Day_11_solver, 69918, "../../input_files/2022/day_11.txt"},
		{2022, 11, 2, Day_11_solver, 19573408701, "../../input_files/2022/day_11.txt"},

		{2022, 12, 1, Day_12_solver, 31, ""},
		{2022, 12, 2, Day_12_solver, 29, ""},
		{2022, 12, 1, Day_12_solver, 456, "../../input_files/2022/day_12.txt"},
		{2022, 12, 2, Day_12_solver, 454, "../../input_files/2022/day_12.txt"},
	}

	for _, exp := range expectations {
		test_expectation(t, exp)
	}
}

func test_expectation(t *testing.T, e expectation) {
	var path string
	path = aocgeneric.Generate_day_path(e.year, e.day, true)
	if e.override_path != "" {
		path = e.override_path
	}

	result := e.solver(path, e.part)
	if result != e.result {
		t.Log("Issues with solver for day: " + fmt.Sprint(e.day) + " part: " + fmt.Sprint(e.part))
		t.Log("The expected value was: " + fmt.Sprint(e.result) + " but the solver returned: " + fmt.Sprint(result))
		t.Fail()
	}
}

func Test_day_5_solver(t *testing.T) {
	if Day_5_solver("../../input_files/2022/day_5_test.txt", 1) != "CMZ" {
		t.Log("Fails to generate expected output from example")
		t.Fail()
	}
	if Day_5_solver("../../input_files/2022/day_5_test.txt", 2) != "MCD" {
		t.Log("Fails to generate expected output from example")
		t.Fail()
	}
}
