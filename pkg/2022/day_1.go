package aoc2022

import (
	"maths"
	"parse/text"
	"sort"
	"strconv"
)

func Day_1_solver(_source_path string, _part int) int {
	fileLines, err := text.Row_to_string_file_parser(_source_path)
	if err != nil {
		return 0
	}

	totals := make([]int, len(fileLines))
	elf_number := 0
	for _, val := range fileLines {
		if val == "" {
			elf_number++
		} else {
			int_val, _ := strconv.Atoi(val)
			totals[elf_number] += int_val
		}
	}
	if _part == 1 {
		return maths.Max(totals)
	}

	if _part == 2 {

		// get the total of the best 3 elves
		var new_totals []int = totals
		sort.Ints(new_totals)
		return maths.Sum(new_totals[len(new_totals)-3:])
	}

	return 0
}
