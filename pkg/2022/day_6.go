package aoc2022

import (
	"parse/text"
	"set"
	"strings"
)

func Day_6_solver(_source_path string, _part int) int {
	parsed_input, _ := text.Row_to_string_file_parser(_source_path)
	raw_signal := parsed_input[0]
	split_signal := strings.Split(raw_signal, "")

	window := []string{}
	if _part == 1 {

		for key, val := range split_signal {
			if len(window) < 4 {
				window = append(window, val)
			} else {
				window = append(window[1:], val)
			}
			if len(set.Unique(window)) == 4 {
				return key + 1
			}

		}
	}

	if _part == 2 {

		for key, val := range split_signal {
			if len(window) < 14 {
				window = append(window, val)
			} else {
				window = append(window[1:], val)
			}
			if len(set.Unique(window)) == 14 {
				return key + 1
			}

		}
	}

	return 0
}
