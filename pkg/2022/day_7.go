package aoc2022

import (
	"maths"
	"parse/text"
	"regexp"
	"strconv"
)

type dir struct {
	name    string
	parent  string
	subdirs map[string]dir
	files   map[string]int
}

func Day_7_solver(_source_path string, _part int) int {
	rows, _ := text.Row_to_string_file_parser(_source_path)

	root := dir{
		"/",
		"",
		make(map[string]dir),
		make(map[string]int),
	}
	current_name := "/"

	for key, row := range rows {
		if key == 0 { // skip the first row
			continue
		}

		switch {
		case command(row):
			if ls(row) {
				// fmt.Println("found ls")
			} else if cd(row) {
				// fmt.Println("found cd, going to ", cd_destinaition(row))
				if cd_destinaition(row) == ".." {
					current_name = root.subdirs[current_name].parent
					continue
				}
				current_name += ("/" + cd_destinaition(row))

			}
		case directory(row):
			// fmt.Println("found dir, called", directory_name(row))
			root.subdirs[current_name+"/"+directory_name(row)] = dir{
				current_name + "/" + directory_name(row),
				current_name,
				make(map[string]dir),
				make(map[string]int),
			}

		case file(row):
			// fmt.Println("found file with size:", file_size(row), "and name", file_name(row), "at", current_name)
			if current_name == "/" {
				root.files[file_name(row)] = file_size(row)
				continue
			}
			root.subdirs[current_name].files[file_name(row)] = file_size(row)
		}

	}

	// fmt.Println(root)
	if _part == 1 {
		output := 0
		for _, subdir := range root.subdirs {
			if size(subdir, root) <= 100000 {
				output += size(subdir, root)
			}
		}
		return output
	}

	if _part == 2 {

		total_filesystem_size := 70000000

		used_space := size(root, root)

		needed_space := 30000000 - (total_filesystem_size - used_space)

		candidate_sizes := []int{used_space}
		for _, dir := range root.subdirs {
			dir_size := size(dir, root)
			if dir_size > needed_space {
				candidate_sizes = append(candidate_sizes, dir_size)
			}
		}
		return maths.Min(candidate_sizes)
	}

	return 0
}

func command(_input string) bool {
	return regexp.MustCompile(`\$.*`).Match([]byte(_input))
}

func ls(_input string) bool {
	return regexp.MustCompile(`\$ ls.*`).Match([]byte(_input))
}

func cd(_input string) bool {
	return regexp.MustCompile(`\$ cd.*`).Match([]byte(_input))
}

func directory(_input string) bool {
	return regexp.MustCompile(`dir.*`).Match([]byte(_input))
}
func directory_name(_input string) string {
	return regexp.MustCompile(`dir (.*)`).FindStringSubmatch(_input)[1]
}

func file(_input string) bool {
	return regexp.MustCompile(`\d+.*`).Match([]byte(_input))
}

func file_size(_input string) int {
	text_size := regexp.MustCompile(`(\d+).*`).FindStringSubmatch(_input)[1]
	int_size, _ := strconv.Atoi(text_size)
	return int_size
}

func file_name(_input string) string {
	return regexp.MustCompile(`\d+ (.*)`).FindStringSubmatch(_input)[1]
}

func cd_destinaition(_input string) string {
	return regexp.MustCompile(`\$ cd (.*)`).FindStringSubmatch(_input)[1]
}

func size(_input dir, _root_store dir) int {
	var counter int

	for _, file_size := range _input.files {
		counter += file_size
	}

	for _, subdir := range _root_store.subdirs {
		if subdir.parent == _input.name {
			counter += size(subdir, _root_store)
		}
	}
	return counter
}
