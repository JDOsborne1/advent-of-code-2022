package aoc2022

import (
	"parse/text"
	"set"
	"strconv"
	"strings"
)

func Day_4_solver(_source_path string, _part int) int {
	input_lines, err := text.Row_to_string_file_parser(_source_path)
	if err != nil {
		return 0
	}

	elf_pairs := [][]string{}

	for _, val := range input_lines {
		assignments := strings.Split(val, ",")
		elf_pairs = append(elf_pairs, assignments)
	}

	elf_ranges := [][][]int{}
	for _, pair := range elf_pairs {
		paired_range := [][]int{
			text_range_to_int_slice(pair[0]),
			text_range_to_int_slice(pair[1]),
		}
		elf_ranges = append(elf_ranges, paired_range)
	}

	if _part == 1 {

		enclosed_pairs := 0

		for _, val := range elf_ranges {
			if set.Contains(val[0], val[1]) {
				enclosed_pairs++
				continue
			}
			if set.Contains(val[1], val[0]) {
				enclosed_pairs++
				continue
			}
		}
		return enclosed_pairs
	}

	if _part == 2 {
		overlapping_pairs := 0

		for _, pair := range elf_ranges {
			intersection := set.Intersect(pair[0], pair[1])
			if len(intersection) > 0 {
				overlapping_pairs++
			}
		}

		return overlapping_pairs
	}

	return 0
}

func text_range_to_int_slice(_input string) []int {
	boundaries := strings.Split(_input, "-")
	output := []int{}
	lower_bound, _ := strconv.Atoi(boundaries[0])
	upper_bound, _ := strconv.Atoi(boundaries[1])

	for i := lower_bound; i < upper_bound+1; i++ {
		output = append(output, i)
	}

	return output
}
