package aocgeneric

import "fmt"

func Generate_day_path(_year int, _day int, _test bool) string {
	base_path := "../../input_files/"
	base_path += fmt.Sprint(_year)
	base_path += "/day_"
	base_path += fmt.Sprint(_day)
	if _test {
		base_path += "_test"
	}
	base_path += ".txt"

	return base_path
}
