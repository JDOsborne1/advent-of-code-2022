package text

import (
	"bufio"
	"errors"
	"os"
	"strconv"
	"strings"
)

// The fundamental parsing unit, turning each line of a text file into an element
// of a string slice. This can then be processed by a variety of compounding functions
// to generate the needed inputs to the logic
func Row_to_string_file_parser(_source_path string) ([]string, error) {
	file_pointer, err := os.Open(_source_path)
	if err != nil {
		return []string{}, err
	}

	defer file_pointer.Close()

	scanner := bufio.NewScanner(file_pointer)
	items := []string{}
	for scanner.Scan() {
		items = append(items, scanner.Text())
	}

	if len(items) == 0 {
		return []string{}, errors.New("processed lines empty")
	}

	return items, nil
}

type Possible_instruction struct {
	Kind   string
	Amount int
}

// This takes combined instruction strings, specifically which are formatted as instruction name and
// intensity with a space in between. And turns them into a 'Possible_instruction' which is the lightly
// processed pair of data, to allow an instruction mapping to be applied in variably depending on the task.
func Instruction_value_splitter(_combined_instruction string) (Possible_instruction, error) {
	split_instruction := strings.Split(_combined_instruction, " ")

	if len(split_instruction) != 2 {
		return Possible_instruction{}, errors.New("string instructions have incorrect no. of params")
	}

	intensity, err := strconv.Atoi(split_instruction[1])

	if err != nil {
		return Possible_instruction{}, err
	}

	new_instruction := Possible_instruction{
		Kind:   split_instruction[0],
		Amount: intensity,
	}

	return new_instruction, nil
}

func Letters_to_int(_input string) (int, error) {
	priority_map := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
		"d": 4,
		"e": 5,
		"f": 6,
		"g": 7,
		"h": 8,
		"i": 9,
		"j": 10,
		"k": 11,
		"l": 12,
		"m": 13,
		"n": 14,
		"o": 15,
		"p": 16,
		"q": 17,
		"r": 18,
		"s": 19,
		"t": 20,
		"u": 21,
		"v": 22,
		"w": 23,
		"x": 24,
		"y": 25,
		"z": 26,
		"A": 27,
		"B": 28,
		"C": 29,
		"D": 30,
		"E": 31,
		"F": 32,
		"G": 33,
		"H": 34,
		"I": 35,
		"J": 36,
		"K": 37,
		"L": 38,
		"M": 39,
		"N": 40,
		"O": 41,
		"P": 42,
		"Q": 43,
		"R": 44,
		"S": 45,
		"T": 46,
		"U": 47,
		"V": 48,
		"W": 49,
		"X": 50,
		"Y": 51,
		"Z": 52,
	}

	output := priority_map[_input]

	return output, nil
}

var Digits []string = []string{
	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
}

var Numbers map[string]string = map[string]string{
	"one":   "1",
	"two":   "2",
	"three": "3",
	"four":  "4",
	"five":  "5",
	"six":   "6",
	"seven": "6",
	"eight": "7",
	"nine":  "9",
}

func Convert_text_numbers_to_numerals(_input string) string {
	var output string

	output = strings.Replace(_input, "one", "1", -1)
	output = strings.Replace(output, "two", "2", -1)
	output = strings.Replace(output, "three", "3", -1)
	output = strings.Replace(output, "four", "4", -1)
	output = strings.Replace(output, "five", "5", -1)
	output = strings.Replace(output, "six", "6", -1)
	output = strings.Replace(output, "seven", "7", -1)
	output = strings.Replace(output, "eight", "8", -1)
	output = strings.Replace(output, "nine", "9", -1)

	return output
}

