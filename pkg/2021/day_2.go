package aoc2021

import (
	"functional"
	"parse/text"
	"submarine"
)

func Day_2_solver(_source_path string, _part int) int {
	parsed_file, err := text.Row_to_string_file_parser(_source_path)
	if err != nil {
		return 0
	}
	sub := new(submarine.Sub)

	if _part == 2 {
		instructions, err := functional.Map_instruct(parsed_file, d2p2_instructions)
		if err != nil {
			return 0
		}
		for _, instruction := range instructions {
			sub.Execute(instruction)
		}

		return sub.Depth * sub.Horizontal
	}

	instructions, err := functional.Map_instruct(parsed_file, d2p1_instructions)
	if err != nil {
		return 0
	}
	for _, instruction := range instructions {
		sub.Execute(instruction)
	}

	return sub.Depth * sub.Horizontal
}
