package aoc2021

import (
	"functional"
	"parse/text"
	"strconv"
)

func Day_1_solver(_source_path string, _part int) int {
	parsed_file, err := text.Row_to_string_file_parser(_source_path)
	if err != nil {
		return 0
	}

	int_input, err := functional.Map_int(parsed_file, strconv.Atoi)

	if err != nil {
		return 0
	}
	if _part == 2 {
		return count_three_item_window_increases(int_input)
	}
	return count_single_value_increases(int_input)
}
