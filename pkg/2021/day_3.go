package aoc2021

import (
	"binary"
	"fmt"
	"functional"
	"matrix"
	"parse/text"
	"stats"
	"strconv"
	"strings"
)

func remove_based_on_most_common(_input [][]int, _most_common []int, _at_index int) ([][]int, error) {

	reduced_data, err := functional.Filter_slice(_input, func(input_ []int) (bool, error) {
		if input_[_at_index] == _most_common[_at_index] {
			return true, nil
		}
		return false, nil
	})

	if err != nil {
		return [][]int{}, err
	}
	return reduced_data, nil
}

func keep_modal_values(_from [][]int, _at_index int) ([][]int, error) {
	vertical_data := matrix.Transpose(_from)
	fmt.Println("from keep_modal_value: vertical data:", vertical_data)

	most_common_value, err := functional.Map_int(vertical_data, stats.Mode)
	if err != nil {
		return [][]int{}, err
	}
	fmt.Println("from keep_modal_value: most_common_value:", most_common_value)

	reduced_data, err := remove_based_on_most_common(_from, most_common_value, _at_index)

	if err != nil {
		return [][]int{}, err
	}

	return reduced_data, nil
}

func Day_3_solver(_source_path string, _part int) int {
	parsed_file, err := text.Row_to_string_file_parser(_source_path)
	if err != nil {
		return 0
	}

	// Generate list of binary numbers from strings
	split_binary := [][]int{}
	var split_string []string
	var parsed_split_string []int

	for _, value := range parsed_file {
		split_string = strings.Split(value, "")
		parsed_split_string, err = functional.Map_int(split_string, strconv.Atoi)
		split_binary = append(split_binary, parsed_split_string)
		if err != nil {
			return 0
		}
	}

	if _part == 1 {
		vertical_data := matrix.Transpose(split_binary)

		most_common_value, err := functional.Map_int(vertical_data, stats.Mode)
		if err != nil {
			return 0
		}

		gamma_rate := binary.Binary_to_decimal(most_common_value)
		epsilon_rate := binary.Binary_to_decimal(binary.Invert(most_common_value))

		return gamma_rate * epsilon_rate
	}

	if _part == 2 {
		reduced_data := split_binary

		reduced_data, _ = keep_modal_values(reduced_data, 0)
		reduced_data, _ = keep_modal_values(reduced_data, 1)
		fmt.Println("third list: ", reduced_data, "index: ", 2)
		reduced_data, err = keep_modal_values(reduced_data, 2)
		if err != nil {
			fmt.Println(err)
			return 0
		}
		fmt.Println("fourth list: ", reduced_data, "index: ", 3)

		return 0
	}

	return 0
}
