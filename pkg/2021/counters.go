package aoc2021

// function to compare values in a list with the last one, and count the increases
// omits the first value to create the window, as simpler than omitting final one
func count_single_value_increases(_input []int) int {
	var count_increases int
	for key := range _input {
		if key == 0 {
			continue
		}
		increase_here := _input[key] > _input[key-1]

		if increase_here {
			count_increases++
		}
	}

	return count_increases
}

// Function to compare a rolling window of 3 values with a window 1 step back
// and counts the number of increases over that average. Similar to the single
// value counter, this skips the first 3 values to make a suitable grouping
func count_three_item_window_increases(_input []int) int {
	var count_increases int
	min_key := 3
	max_key := len(_input)

	for key := min_key; key < max_key; key++ {
		if key == 0 {
			continue
		}
		current_window := _input[key] + _input[key-1] + _input[key-2]
		last_window := _input[key-1] + _input[key-2] + _input[key-3]
		increase_here := current_window > last_window

		if increase_here {
			count_increases++
		}
	}

	return count_increases
}
