package aoc2021

import (
	"errors"
	"parse/text"
	"submarine"
)

func d2p1_instructions(_proto_instruction string) (submarine.Instruction, error) {
	text_instruction, err := text.Instruction_value_splitter(_proto_instruction)
	if err != nil {
		return submarine.Instruction{}, nil
	}
	if text_instruction.Kind == "forward" {
		return submarine.Instruction{Kind: submarine.Move_horizontal, Intensity: text_instruction.Amount}, nil
	}
	if text_instruction.Kind == "up" {
		return submarine.Instruction{Kind: submarine.Move_up, Intensity: text_instruction.Amount}, nil
	}
	if text_instruction.Kind == "down" {
		return submarine.Instruction{Kind: submarine.Move_down, Intensity: text_instruction.Amount}, nil
	}

	return submarine.Instruction{}, errors.New("unmapped instruction")

}

func d2p2_instructions(_proto_instruction string) (submarine.Instruction, error) {
	text_instruction, err := text.Instruction_value_splitter(_proto_instruction)
	if err != nil {
		return submarine.Instruction{}, nil
	}
	if text_instruction.Kind == "forward" {
		return submarine.Instruction{Kind: submarine.Move_horizontal, Intensity: text_instruction.Amount}, nil
	}
	if text_instruction.Kind == "up" {
		return submarine.Instruction{Kind: submarine.Aim_up, Intensity: text_instruction.Amount}, nil
	}
	if text_instruction.Kind == "down" {
		return submarine.Instruction{Kind: submarine.Aim_down, Intensity: text_instruction.Amount}, nil
	}

	return submarine.Instruction{}, errors.New("unmapped instruction")

}
