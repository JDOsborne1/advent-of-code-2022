package aoc2021

import (
	"aocgeneric"
	"fmt"
	"testing"
)

type expectation struct {
	year          int
	day           int
	part          int
	solver        func(string, int) int
	result        int
	override_path string
}

func Test_Solvers(t *testing.T) {

	var expectations []expectation = []expectation{
		{2021, 1, 1, Day_1_solver, 7, ""},
		{2021, 1, 2, Day_1_solver, 5, ""},
		{2021, 1, 1, Day_1_solver, 1228, "../../input_files/2021/day_1.txt"},
		{2021, 1, 2, Day_1_solver, 1257, "../../input_files/2021/day_1.txt"},

		{2021, 2, 1, Day_2_solver, 150, ""},
		{2021, 2, 2, Day_2_solver, 900, ""},
		{2021, 2, 1, Day_2_solver, 1694130, "../../input_files/2021/day_2.txt"},
		{2021, 2, 2, Day_2_solver, 1698850445, "../../input_files/2021/day_2.txt"},

		{2021, 3, 1, Day_3_solver, 198, ""},
	}

	for _, exp := range expectations {
		test_expectation(t, exp)
	}
}

func test_expectation(t *testing.T, e expectation) {
	var path string
	path = aocgeneric.Generate_day_path(e.year, e.day, true)
	if e.override_path != "" {
		path = e.override_path
	}

	result := e.solver(path, e.part)
	if result != e.result {
		t.Log("Issues with solver for day: " + fmt.Sprint(e.day) + " part: " + fmt.Sprint(e.part))
		t.Log("The expected value was: " + fmt.Sprint(e.result) + " but the solver returned: " + fmt.Sprint(result))
		t.Fail()
	}
}
