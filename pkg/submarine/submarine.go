package submarine

// The data type for instructions which can be actioned on a Sub. These actions are all state changes of
// various kinds, with their intensities controlled by the intensity parameter.
type Instruction struct {
	Kind      func(*Sub, int)
	Intensity int
}

// The state machine to represent the submarine in the 2021 story
type Sub struct {
	Depth      int
	Horizontal int
	Aim        int
}

func (s *Sub) Execute(_used_instruction Instruction) {
	_used_instruction.Kind(s, _used_instruction.Intensity)
}

func Move_up(_submarine *Sub, _amount int) {
	_submarine.Depth -= _amount
}

func Move_down(_submarine *Sub, _amount int) {
	_submarine.Depth += _amount
}

func Aim_up(_submarine *Sub, _amount int) {
	_submarine.Aim -= _amount
}

func Aim_down(_submarine *Sub, _amount int) {
	_submarine.Aim += _amount
}

func Move_horizontal(_submarine *Sub, _amount int) {
	_submarine.Horizontal += _amount
	_submarine.Depth += (_submarine.Aim * _amount)
}
