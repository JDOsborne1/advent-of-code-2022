package set

import "golang.org/x/exp/maps"

func Intersect[t comparable](_slice_a []t, _slice_b []t) []t {
	member_map := make(map[t]bool)
	var results []t
	for _, val := range _slice_a {
		member_map[val] = true
	}

	for _, val := range _slice_b {
		if member_map[val] {
			results = append(results, val)
		}
	}

	return Unique(results)
}

func Unique[t comparable](_input []t) []t {
	output := make(map[t]bool)
	for _, val := range _input {
		output[val] = true
	}

	return maps.Keys(output)
}

// Function to determine if _set_a fully contains set_b
func Contains[t comparable](_set_a []t, _set_b []t) bool {
	outsiders_b := Outsiders(_set_a, _set_b)

	return len(outsiders_b) == 0
}

// Function to determine the outsiders of the first set which are in the second
func Outsiders[t comparable](_of []t, _in []t) []t {
	member_map := make(map[t]bool)
	var results []t
	for _, val := range _of {
		member_map[val] = true
	}

	for _, val := range _in {
		if !member_map[val] {
			results = append(results, val)
		}
	}

	return Unique(results)
}

func Antisect[t comparable](_set_a []t, _set_b []t) []t {
	bi_directional_outsiders := append(Outsiders(_set_a, _set_b), Outsiders(_set_b, _set_a)...)

	return Unique(bi_directional_outsiders)
}

func Reverse[t any](_input []t) []t {
	size := len(_input)
	output := make([]t, size)

	for key, val := range _input {
		inv_key := size - key - 1
		output[inv_key] = val
	}

	return output
}

// > Append always tries to modify the underlying array.
// - some nutter in the core go team
func Identity(_input []string) []string {
	new_clone := []string{}

	new_clone = append(new_clone, _input...)

	return new_clone
}

// Function to determine if set a contans object b
func Member[t comparable](_set_a []t, _b t) bool {
	for _, val := range _set_a {
		if val == _b {
			return true
		}
	}

	return false
}
