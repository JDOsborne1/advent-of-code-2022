package matrix

import (
	"errors"
	"fmt"
)

func Transpose[t comparable](_input [][]t) [][]t {
	var first_order_length int
	var second_order_length int
	first_order_length = len(_input)
	if len(_input) != 0 {
		second_order_length = len(_input[0])
	}

	result := make([][]t, second_order_length)

	for key := range result {
		result[key] = make([]t, first_order_length)
	}

	for i := 0; i < second_order_length; i++ {
		for j := 0; j < first_order_length; j++ {
			if len(_input) >= j+1 && len(_input[j]) >= i+1 {
				result[i][j] = _input[j][i]
			}
		}

	}

	return result
}

// func keep_most_common_from_vertical(_input_list_of_binaries [][]int, _most_common_binary []int, _index int) [][]int {
// 	result := [][]int{}
// 	for _, value := range _input_list_of_binaries {
// 		if value[_index] == _most_common_binary[_index] {
// 			result = append(result, value)
// 		}
// 	}
// 	return result
// }

var ErrOutOfBounds = errors.New("out of the boundaries of the matrix")

func Coord_get(_input [][]string, _coord []int) (string, error) {

	if len(_coord) != 2 {
		return "", errors.New("incorrect coordinate length")
	}

	if len(_input) == 0 {
		return "", nil
	}

	if _coord[0] >= len(_input) || _coord[0] < 0 {
		return "", fmt.Errorf("exceeds Vertical bounds: %w", ErrOutOfBounds)
	}

	// Assumes is a matrix, each line is same width
	if _coord[1] >= len(_input[0]) || _coord[1] < 0 {
		return "", fmt.Errorf("exceeds horizontal bounds: %w", ErrOutOfBounds)
	}

	return _input[_coord[0]][_coord[1]], nil

}

func Get_around(_input [][]string, _coord []int) ([][]string, error) {
	rows := []int{_coord[0] - 1, _coord[0], _coord[0] + 1}
	cols := []int{_coord[1] - 1, _coord[1], _coord[1] + 1}

	output := [][]string{}

	for _, row := range rows {
		entry := []string{}
		for _, col := range cols {
			val, err := Coord_get(_input, []int{row, col})
			if errors.Is(err, ErrOutOfBounds) {
				continue
			}
			entry = append(entry, val)
		}
		if len(entry) == 0 {
			continue
		}
		output = append(output, entry)
	}

	return output, nil

}

func Render_matrix(_input [][]string) {
	for _, row := range _input {
		fmt.Println(row)
	}
}
