package topography

import (
	"testing"
)

func Test_visibility_func(t *testing.T) {
	input_1 := []int{3, 0, 3, 7, 3}
	expected_left := []bool{true, false, false, true, false}
	expected_right := []bool{false, false, false, true, true}

	for key := range input_1 {
		if Left_visible(input_1, key) != expected_left[key] {
			t.Log("Left: expected", expected_left[key], "at position", key, "and got: ", Left_visible(input_1, key))
			t.Fail()
		}
		if Right_visible(input_1, key) != expected_right[key] {
			t.Log("right: expected", expected_right[key], "at position", key, "and got: ", Right_visible(input_1, key))
			t.Fail()
		}
	}
}

func Test_scenic_func(t *testing.T) {
	input_1 := []int{3, 0, 3, 7, 3}
	expected_left := []int{0, 1, 2, 3, 1}
	expected_right := []int{2, 1, 1, 1, 0}

	for key := range input_1 {

		if Left_scenic(input_1, key) != expected_left[key] {
			t.Log("left: expected", expected_left[key], "at position", key, "and got: ", Left_scenic(input_1, key))
			t.Fail()
		}

		if Right_scenic(input_1, key) != expected_right[key] {
			t.Log("right: expected", expected_right[key], "at position", key, "and got: ", Right_scenic(input_1, key))
			t.Fail()
		}
	}
}
