package topography

import "set"

// Is the given position in the input visible from the left of the axis
func Left_visible(_input []int, _position int) bool {
	visible := true
	reference := _input[_position]
	for key, val := range _input {
		if key == _position {
			return visible
		}
		if val >= reference {
			visible = false
		}
	}

	return visible
}

// Is the given position in the input visible from the right of the axis
func Right_visible(_input []int, _position int) bool {
	visible := true
	reversed_input := set.Reverse(_input)
	reversed_position := (len(_input) - _position) - 1
	reference := reversed_input[reversed_position]
	for key, val := range reversed_input {
		if key == reversed_position {
			return visible
		}
		if val >= reference {
			visible = false
		}
	}

	return visible
}

// How many units to the right can be seen from the position
func Right_scenic(_input []int, _position int) int {
	score := 0
	reference := _input[_position]
	for key, val := range _input {
		if key <= _position {
			continue
		}
		score++
		if val >= reference {
			break
		}
	}

	return score
}

// How many units to the left can be seen from the position
func Left_scenic(_input []int, _position int) int {
	score := 0
	reversed_input := set.Reverse(_input)
	reversed_position := (len(_input) - _position) - 1
	reference := reversed_input[reversed_position]
	for key, val := range reversed_input {
		if key <= reversed_position {
			continue
		}
		score++
		if val >= reference {
			break
		}
	}

	return score
}
