package functional

import "testing"

func Test_filter_using_matricies(t *testing.T) {
	example_matrix := [][]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	test_matrix1, _ := Filter_slice(example_matrix, func(_input []int) (bool, error) { return false, nil })
	if len(test_matrix1) != 0 {
		t.Log("Doesnt fully remove an always false filter")
		t.Fail()
	}

	test_matrix2, _ := Filter_slice(example_matrix, func(_input []int) (bool, error) { return true, nil })
	if len(test_matrix2) != 3 {
		t.Log("Doesnt full allow an always true filter")
		t.Fail()
	}

	test_matrix3, _ := Filter_slice(example_matrix,
		func(_input []int) (bool, error) {
			if _input[0] != 4 {
				return true, nil
			}
			return false, nil
		})

	if len(test_matrix3) != 2 {
		t.Log("Fails to filter using conditions")
		t.Log(test_matrix3)
		t.Fail()
	}

}
