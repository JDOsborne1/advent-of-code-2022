package functional

import (
	"errors"
	"submarine"
)

// Prototype Mapping function which translates one of its supported source types into an integer.
// This uses the generic inputs & strict outputs model.
func Map_int[t string | int | []int](_input []t, _processor func(t) (int, error)) ([]int, error) {
	result := []int{}
	for _, value := range _input {
		int_value, err := _processor(value)
		if err != nil {
			return []int{}, err
		}
		result = append(result, int_value)
	}

	if len(_input) != len(result) {
		return []int{}, errors.New("mismatched input and result length")
	}

	return result, nil
}

// Prototype Mapping function which translates one of its supported source types into an Instruction set..
// This uses the generic inputs & strict outputs model.
func Map_instruct(_input []string, _processor func(string) (submarine.Instruction, error)) ([]submarine.Instruction, error) {
	result := []submarine.Instruction{}
	for _, value := range _input {
		instruction_value, err := _processor(value)
		if err != nil {
			return []submarine.Instruction{}, err
		}
		result = append(result, instruction_value)
	}

	if len(_input) != len(result) {
		return []submarine.Instruction{}, errors.New("mismatched input and result length")
	}

	return result, nil
}

func Filter_slice[t int | []int | string](_input []t, _filter func(t) (bool, error)) ([]t, error) {
	output := []t{}

	for _, val := range _input {
		keep, err := _filter(val)
		if err != nil {
			return []t{}, err
		}
		if keep {
			output = append(output, val)
		}
	}
	return output, nil
}

func Reduce[t any](_input []t, _reducer func(t, t) t) t {
	var output t
	if len(_input) == 0 {
		return output
	}

	if len(_input) == 1 {
		return _input[0]
	}

	if len(_input) == 2 {
		return _reducer(_input[0], _input[1])
	}

	max_iterations := len(_input) - 1
	for i := 0; i < max_iterations; i++ {
		reduced := _reducer(_input[0], _input[1]) // the reduced value of the first two elements
		if len(_input) == 2 {
			return _reducer(_input[0], _input[1])
		}
		_input = append([]t{reduced}, _input[2:]...) // add that reduced value to the remaining slice
	}

	return _input[0]
}
