package stats

import (
	"errors"
	"github.com/montanaflynn/stats"
)

func Mode(_input []int) (int, error) {
	float_input := []float64{}

	for _, value := range _input {
		float_input = append(float_input, float64(value))
	}

	mode, err := stats.Mode(float_input)
	if err != nil {
		return 0, err
	}

	if len(mode) == 0 {
		return 0, errors.New("mode generates a 0 length slice")
	}

	int_mode := int(mode[0])

	return int_mode, nil
}
