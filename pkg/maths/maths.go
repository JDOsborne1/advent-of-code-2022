package maths

func Max(_input []int) int {
	if len(_input) == 0 {
		return 0
	}
	init := _input[0]
	for _, val := range _input {
		if val > init {
			init = val
		}
	}
	return init
}

func Min(_input []int) int {
	if len(_input) == 0 {
		return 0
	}

	init := _input[0]
	for _, val := range _input {
		if val < init {
			init = val
		}
	}
	return init
}

func Sum(_input []int) int {
	output := 0
	for _, val := range _input {
		output += val
	}
	return output
}

func Abs(_input int) int {
	if _input < 0 {
		return -_input
	}
	return _input
}

func Prod(_input []int) int {
	init := 1
	for _, val := range _input {
		init *= val
	}
	return init
}
